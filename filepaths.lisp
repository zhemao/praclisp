(in-package :cl-user)

(defpackage #:fpath
  (:use :cl)
  (:export #:join #:dirname #:basename #:split #:splitext))

(in-package :fpath)

(defparameter *pathsep* 
  (if (string= (software-type) "Windows")
    #\\ #\/))

(defparameter *extsep* #\.)

(defun join (paths)
  (let* ((firstpath (car paths))
         (following (cdr paths))
         (lastchar (elt firstpath (1- (length firstpath)))))
    (if (null following)
      firstpath
      (if (char= lastchar *pathsep*)
        (concatenate 'string firstpath (join following))
        (concatenate 'string firstpath 
                     (string *pathsep*) 
                     (join following))))))

(defun split-by-char (str chr)
  (let ((seppos (position chr str :from-end t)))
    (if seppos 
      (list (subseq str 0 seppos) (subseq str (1+ seppos)))
      (list str ""))))

(defun split (path)
  (split-by-char path *pathsep*))

(defun dirname (path)
  (first (split path)))

(defun basename (path)
  (second (split path)))

(defun splitext (path)
  (split-by-char path *extsep*))
