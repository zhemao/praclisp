(in-package :cl-user)

(defpackage #:trial
  (:use :cl)
  (:export #:deftest #:check #:combine-results))

(in-package :trial)

(defvar *curtest* nil)

(defun report-result (result form)
  (format t "~:[FAIL~;pass~] ... ~a: ~a~%" 
          result *curtest* form)
  result)

(defmacro check (&body forms)
  `(combine-results
     ,@(loop for f in forms collect `(report-result ,f ',f))))

(defmacro combine-results (&body forms)
  (let ((result (gensym)))
    `(let ((,result t))
       ,@(loop for f in forms collect `(unless ,f (setf ,result nil)))
       ,result)))

(defmacro deftest (name parameters &body body)
  `(defun ,name ,parameters 
     (setf *curtest* ',name)
     ,@body))
