(in-package :cl-user)

(defpackage #:mrsbun
  (:use :cl :cl-ppcre :fad)
  (:export #:make-filter #:classify #:train))

(in-package :mrsbun)

(defun make-filter ()
  (let ((filter (make-hash-table)))
    (setf (gethash :spam filter) (make-hash-table :test 'equal))
    (setf (gethash :ham filter) (make-hash-table :test 'equal))
    filter))

(defun check-category (category)
  (when (not (or (eql category :spam) (eql category :ham)))
    (error "Category must be :spam or :ham")))

(defun add-word (filter category word)
  (check-category category)
  (let ((cathash (gethash category filter)))
    (if (null (gethash word cathash)) 
      (setf (gethash word cathash) 1)
      (setf (gethash word cathash) (1+ (gethash word cathash))))
    (setf (gethash category filter) cathash)
    filter))

(defun opposite-category (category)
  (if (eql category :spam) :ham :spam))

(defun get-word-probability (filter category word)
  (check-category category)
  (let* ((opposite (opposite-category category))
         (word-count-cat (gethash word (gethash category filter) 0))
         (word-count-op (gethash word (gethash opposite filter) 0))
         (cat-count (loop for v being the hash-values 
                          in (gethash category filter) 
                          summing v))
         (op-count (loop for v being the hash-values
                         in (gethash opposite filter)
                         summing v))
         (word-prob-cat (/ word-count-cat cat-count))
         (cat-prob (/ cat-count (+ cat-count op-count)))
         (word-prob-total (/ (+ word-count-cat word-count-op) 
                             (+ cat-count op-count))))
    (/ (* word-prob-cat cat-prob) word-prob-total)))

(defun get-probability (filter category words)
  (apply #'* 
    (loop for word in words 
          collect (get-word-probability filter category word))))

(defun extract-words (message)
  (delete-duplicates 
    (all-matches-as-strings "[a-zA-Z]+" message)
    :test #'string=))

(defun extract-words-from-file (fname)
  (with-open-file (stream fname)
    (reduce #'union 
      (loop for line = (read-line stream nil) 
            while line collect (extract-words line)))))

(defun train-file (filter category fname)
  (loop for word in (extract-words-from-file fname)
        do (add-word filter category word))
  filter)

(defun train (filter spam-folder ham-folder)
  (unless 
    (directory-exists-p spam-folder)
    (error "Could not find spam directory"))
  (unless
    (directory-exists-p ham-folder)
    (error "Could not find ham directory"))
  (let ((spam-files (list-directory spam-folder))
        (ham-files (list-directory ham-folder)))
    (loop for fname in spam-files 
          do (train-file filter :spam fname))
    (loop for fname in ham-files
          do (train-file filter :ham fname))
    filter))

(defun classify (filter fname)
  (let* ((words (extract-words-from-file fname))
         (spamprob (get-probability filter :spam words))
         (hamprob (get-probability filter :ham words)))
    (if (> spamprob hamprob) :spam :ham)))
