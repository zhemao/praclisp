(require "cl-fad")
(require "cl-ppcre")
(load "mrsbun.lisp")
(load "trial.lisp")

(defun check-classifier (filter category folder)
  (loop for fname in (fad:list-directory folder)
        do (trial:check 
             (eql (mrsbun:classify filter fname) category))))

(trial:deftest filter-test ()
  (let ((filter (mrsbun:make-filter)))
    (format t "Training classifier~%")
    (mrsbun:train filter "testmessages/spam" "testmessages/ham")
    (format t "Classifying spam~%")
    (check-classifier filter :spam "testmessages/spam")
    (format t "Classifying ham~%")
    (check-classifier filter :ham "testmessages/ham")))


